//
//  LogsTableViewController.swift
//  Leaderboards-Client
//
//  Created by Tinkertanker on 20/1/20.
//  Copyright © 2020 Tinkertanker. All rights reserved.
//

import UIKit
import FirebaseDatabase

class LogsTableViewController: UITableViewController {
    var logs: [[String:String]] = []
    var ref: DatabaseReference!
    var code: String!
    var doNotAnimate: [IndexPath:Bool] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.alwaysBounceVertical = false
        self.ref = Database.database().reference()
        
        self.ref.child("logs").child(String(code)).observe(.childAdded) { snapshot in
            
            if snapshot.exists() {
                let value = snapshot.value as! [String:String]
                self.logs.append(value)
            } else {
                self.logs = []
            }
            self.tableView.reloadData()
        }
        self.ref.child("logs").child(String(code)).observeSingleEvent(of: .value) { snapshot in
            if snapshot.exists() {
                
                let value = snapshot.value as! [[String:String]]
                self.logs = value
                
            } else {
                self.logs = []
            }
            
            self.tableView.reloadData()
        }
        setUpNavBar()
    }
    func setUpNavBar() {
        self.navigationItem.leftBarButtonItem?.tintColor = #colorLiteral(red: 0.4120000005, green: 0.5289999843, blue: 0.7879999876, alpha: 1)
        
    }
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = false
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return logs.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.height / 5
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "logCell", for: indexPath) as! LogTableViewCell
        cell.selectionStyle = .none
        let log = logs[indexPath.row]
        cell.alpha = 0
        cell.reasonLabel.text = log["reason"]!
        let change = log["change"]!
        if let intChange = Int(change) {
            if intChange < 0 {
                cell.changeLabel.textColor = #colorLiteral(red: 1, green: 0.4117647059, blue: 0.3803921569, alpha: 1)
                cell.changeLabel.text = "\(change) to \(log["group"]!)"
            } else if intChange == 0 {
                cell.changeLabel.textColor = #colorLiteral(red: 0.9919999838, green: 0.9919999838, blue: 0.5879999995, alpha: 1)
                cell.changeLabel.text = "\(change) to \(log["group"]!)"
            } else {
                cell.changeLabel.textColor = #colorLiteral(red: 0.4666666667, green: 0.8666666667, blue: 0.4666666667, alpha: 1)
                cell.changeLabel.text = "+\(change) to \(log["group"]!)"
            }
        } else {
            cell.changeLabel.text = "Missing Data"
        }
        
        let df = DateFormatter()
        let distantPast = df.string(from: .distantPast)
        let locale = Locale(identifier: "en_US_POSIX")
        df.locale = locale
        df.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
        let date = df.date(from: log["date"] ?? distantPast) ?? Date.distantPast
        df.dateFormat = "dd/MM/yyyy\nHH:mm:ss"
        let formattedDate = df.string(from: date)
        cell.timeLabel.text = formattedDate
        
        
        // Configure the cell...
        
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
