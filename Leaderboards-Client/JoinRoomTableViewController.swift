//
//  JoinRoomTableViewController.swift
//  Leaderboards
//
//  Created by Sean Wong on 3/11/19.
//  Copyright © 2019 Tinkertanker. All rights reserved.
//

import UIKit
import FirebaseDatabase
import DictionaryCoding
import DeepDiff
class JoinRoomTableViewController: UITableViewController, BoardDelegate {
    var ref: DatabaseReference!
    @IBOutlet weak var textField6: BoardTextField!
    @IBOutlet weak var textField5: BoardTextField!
    @IBOutlet weak var textField4: BoardTextField!
    @IBOutlet weak var textField3: BoardTextField!
    @IBOutlet weak var textField2: BoardTextField!
    @IBOutlet weak var textField1: BoardTextField!
    var textFields: [UITextField]!
    var currentSelectedField: UITextField!
    @IBOutlet weak var codeStackView: UIStackView!
    var sentRoom: Room!
    
    // MARK: Delegate method for overriding backwards key
    func goBackwards() {
        let indx = textFields.firstIndex(of: currentSelectedField)! // We know that this will always not be nil as there are 6 textFields
        if indx > 0 {
            if currentSelectedField.text != "" {
                currentSelectedField.text = ""
            }
            textFields[indx - 1].becomeFirstResponder()
        } else {
            currentSelectedField.text = ""
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = Database.database().reference()
        textFields = [textField1,textField2, textField3, textField4, textField5, textField6]
        setUpCodeStackView()
        setUpTextField()
    }
    
    func setUpCodeStackView() {
        for textField in codeStackView.arrangedSubviews {
            textField.layer.cornerRadius = 12
            textField.clipsToBounds = true
        }
    }
    
    
    // MARK: - Text field
    func setUpTextField() {
        
        for textfield in textFields {
            textfield.delegate = self
            textfield.tintColor = .white
            textfield.keyboardType = .numberPad
            textfield.addTarget(self, action: #selector(editingChanged(_:)), for: .editingChanged)
            textfield.addTarget(self, action: #selector(editingDidStart(_:)), for: .editingDidBegin)
        }
    }
    
    @objc func editingDidStart(_ textField: UITextField) {
        currentSelectedField = textField
    }
    
    @objc func editingChanged(_ textField: UITextField) {
        let indx = textFields.firstIndex(of: textField)! // We know that this will always not be nil as there are 6 textFields
        if textField.text!.count > 1 {
            let chr = textField.text![1]
            textField.text = chr
            
        }
        if indx == 5 {
            textField6.resignFirstResponder()
            currentSelectedField = textField1
            getRoom()
        } else {
            textFields[indx + 1].becomeFirstResponder()
        }

    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func getRoom() {
        
        // Get room code
        var code = ""
        for textfield in textFields {
            code += textfield.text!
        }
       
       // Get room from Firebase
        
        ref.child("rooms").child(code).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                let value = snapshot.value as! NSDictionary
                let name = value["name"] as! String
                let code = value["code"] as! String
                let maxScore = value["maxScore"] as! Int
                let rawGroups = value["groups"] as! [[String:Any]]
                let dictDecoder = DictionaryDecoder()
                var groups: [Group] = []
                for group in rawGroups {
                    groups.append(try! dictDecoder.decode(Group.self, from: group))
                }
                let room = Room(name: name, code: code, groups: groups, maxScore: maxScore)
                self.sentRoom = room
                self.performSegue(withIdentifier: "showRoom", sender: nil)
            } else {
                self.invalidCodeAlert()
            }
        }
    }
    @IBAction func unwindToJoin(for unwindSegue: UIStoryboardSegue, towards subsequentVC: UIViewController) {
        for textField in textFields {
            textField.text = ""
        }
    }
    func invalidCodeAlert() {
        let alert = UIAlertController(title: "Invalid Code", message: "Invalid code entered.\nPlease try again!", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true) {
            for textField in self.textFields {
                textField.text = ""
            }
        }
        
        
        
    }
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        if segue.identifier == "showRoom" {
            let nav = segue.destination as! UINavigationController
            let dest = nav.viewControllers[0] as! ViewRoomsViewController
            dest.room = sentRoom
        }
     }
    
}
