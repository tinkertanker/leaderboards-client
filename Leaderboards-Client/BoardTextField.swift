//
//  Overrides.swift
//  Leaderboards-Client
//
//  Created by Sean Wong on 31/12/19.
//  Copyright © 2019 Tinkertanker. All rights reserved.
//

import Foundation
import UIKit

class BoardTextField: UITextField {
    override func deleteBackward() {
        let del = self.delegate! as! BoardDelegate
        del.goBackwards()
    }
}
protocol BoardDelegate: UITextFieldDelegate {
    func goBackwards()
}
