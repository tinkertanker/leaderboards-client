//
//  RoomViewController.swift
//  Leaderboards
//
//  Created by Sean Wong on 3/11/19.
//  Copyright © 2019 Tinkertanker All rights reserved.
//

import UIKit
import FirebaseDatabase
import DictionaryCoding
import DeepDiff
class ViewRoomsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    // Recieve room from another viewcontroller
    var room: Room!
    var doNotAnimate: [IndexPath:Bool] = [:]
    var ref: DatabaseReference!
    var roomRef: DatabaseReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = Database.database().reference()
        self.roomRef = ref.child("rooms").child(room.code)
        // Initialise UI
        setUpGroups(newGroups: self.room.groups)
        setUpNavBar()
        // Get room updates
        roomRef.observe(.childChanged) { (snapshot) in
            if snapshot.exists() {
                if let newMaxScore = snapshot.value as? Int {
                    self.room.maxScore = newMaxScore
                    self.doNotAnimate.removeAll()
                }
                if let newName = snapshot.value as? String {
                    self.room.name = newName
                    self.setUpTitleLabel()
                }

                
                if let newGroups = snapshot.value as? [[String:Any]] {
                    var groups: [Group] = []
                    let dictDecoder = DictionaryDecoder()
                    for groupData in newGroups {
                        let group = try! dictDecoder.decode(Group.self, from: groupData)
                        groups.append(group)
                    }
                    self.doNotAnimate.removeAll()
                    self.setUpGroups(newGroups: groups)
                    return
                }
                self.tableView.reloadData()
            }
        }
        
    }
    func setUpTitleLabel() {
        self.navigationItem.title = room.name
        self.navigationItem.titleView?.sizeToFit()
    }
    
    @IBAction func logButtonPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "showLog", sender: self)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: nil) { (_) in
            self.doNotAnimate.removeAll()
            self.tableView.reloadData()
        }
    }
    func setUpNavBar() {
        self.navigationItem.title = room.name
        navigationItem.titleView?.sizeToFit()
    }
    
    func setUpGroups(newGroups: [Group]) {
        // Sort the values and match them with the names
        let oldGroups = room.groups
        let sortedGroups = newGroups.sorted { (a, b) -> Bool in
            a.score > b.score
        }
        var changes = diff(old: oldGroups, new: sortedGroups)
        // REMOVE ALL REPLACE CHANGES
        // The diff algorithm spawns another instance of Group so the diff fails and it marks it as a replace
        // We already handle replaces, we just need the moves and deletes, so we simply remove them
        changes.removeAll { (chg) -> Bool in
            chg.replace != nil
        }
        tableView.reload(changes: changes, updateData: {
            self.room.groups = sortedGroups
        }) { _ in
            self.tableView.reloadData()
        }
    }
    

     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        if segue.identifier == "showLog" {
            let dest = segue.destination as! LogsTableViewController
            dest.code = self.room.code
            dest.navigationItem.backBarButtonItem?.title = self.room.name
        }
     }
    
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return room.groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "team", for: indexPath) as! TeamLeaderboardsTableViewCell
        cell.selectionStyle = .none
        // Set up labels
        cell.groupRankLabel.text = String(indexPath.row + 1)
        cell.scoreLabel.text = String(room.groups[indexPath.row].score)
        
        // Update progress indicator
        let maxWidth = cell.coloredBackground.frame.width
        let greenComponent = UIColor(named: "High Color")!.withAlphaComponent(CGFloat(room.groups[indexPath.row].score) / CGFloat(room.maxScore))
        let redComponent = UIColor(named: "Low Color")!.withAlphaComponent((CGFloat(room.maxScore - room.groups[indexPath.row].score) / CGFloat(room.maxScore)))
        cell.progressIndicator.backgroundColor = greenComponent + redComponent
        cell.editProgressIndicatorWidthContraint.constant = maxWidth * CGFloat(room.groups[indexPath.row].score)/CGFloat(room.maxScore)
        if doNotAnimate[indexPath] == nil {
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5, options: .curveEaseIn, animations: {
                self.tableView.layoutIfNeeded()
            }, completion: nil)
            doNotAnimate[indexPath] = true
        }

        // Set up group name
        cell.groupNameLabel.text = room.groups[indexPath.row].name
        cell.alpha = 0
        return cell
    }
    
}
