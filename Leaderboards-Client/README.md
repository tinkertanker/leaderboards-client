# Leaderboards-Client

This is a companion app with Leaderboards - used for devices viewing the leaderboard

## Instructions to future maintainers

Pull the repo, get access from YJ to the Firebase project if you haven't already.
Add the GoogleService-Info.plist if you haven't, then cd to project directory and run `pod install`

## How do I release?

Build an archive of the app, upload to App Store Connect (TK Robot, **not** Tinkertanker) and then upload to TestFlight.